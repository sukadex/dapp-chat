/** @type {import('next').NextConfig} */

const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  env: {
    BUILD_TYPE: "DEV", // or PROD
  },
};

module.exports = nextConfig;
