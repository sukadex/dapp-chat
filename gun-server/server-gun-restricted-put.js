const port = 8000
const Gun = require('gun')

function hasValidToken (msg) {
  return msg && msg && msg.headers && msg.headers.token && msg.headers.token === 'thisIsTheTokenForReals'
}

// const db = Gun.get("dbRoot")
// db.get( "signup" ).map( handleSignupEvent )
// function handleSignupEvent( signup, field ) {
//    // field will = signup...
//    this.get( "ack" ).put( { user_id : "some ID"} );
// }

// // Add listener
Gun.on('opt', function (ctx) {
  if (ctx.once) {
    return
  }
  // Check all incoming traffic
  ctx.on('in', function (msg) {
    const to = this.to
    // restrict put
    if (msg.put) {
      if (hasValidToken(msg)) {
        console.log('writing')
        to.next(msg)
      } else {
        console.log('not writing')
      }
    } else {
      to.next(msg)
    }
  })
})

const server = require('http').createServer(Gun.serve(__dirname))

const gun = Gun({
  localStorage:false,
  web: server
})

// Sync everything
gun.on('out', { get: { '#': { '*': '' } } })

server.listen(port)

console.log('GUN server (restricted put) started on port 8000')
console.log('Use CTRL + C to stop it')
