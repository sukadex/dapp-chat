import { NextPage } from "next";
import { useState } from "react";
import Gun from "gun";
import "gun/sea";
import { GunDB } from "../../gundb";
const gun = Gun("localhost:8000");

interface DataResponse {
  ok?: number;
  pub?: string;
  err?: string;
}

const Register: NextPage = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const userRegister = () => {
    const user = gun.user();
    GunDB.user().create(username, password, (data: DataResponse) => {
      if (data.err) {
        alert(data.err);
        return
      } else {
        console.log("creating user");
        alert(data.pub);
      }
    });
  };
  return (
    <div className="container mx-auto p-28 space-y-2 content-center">
      <div className="bg-gray-100 rounded-sm">
        <span className="block text-sm font-medium text-center text-slate-700 italic">
          Username/Email
        </span>
        <input
          onChange={(e) => setUsername(e.currentTarget.value)}
          value={username}
          type={"text"}
          placeholder="username"
          className="flex-1 text-center text-slate-800 active:bg-transparent focus:bg-transparent text-lg w-full bg-transparent active:outline-none focus:outline-none"
        />
      </div>
      <div className="bg-gray-100 rounded-sm">
        <span className="block text-sm font-medium text-center text-slate-700 italic">
          Password
        </span>
        <input
          onChange={(e) => setPassword(e.currentTarget.value)}
          value={password}
          type={"text"}
          placeholder="* * * * *"
          className="flex-1 text-center text-slate-800 active:bg-transparent focus:bg-transparent text-lg w-full bg-transparent active:outline-none focus:outline-none"
        />
      </div>
      <div className="content-center float-right">
        <button
          className="bg-cyan-700 rounded-sm w-80"
          onClick={() => userRegister()}
        >
          Register
        </button>
      </div>
    </div>
  );
};

export default Register;
