import { NextPage } from "next";
import { useEffect, useState } from "react";
// import Gun, { GunUser, ISEAPair } from "gun";
// import "gun/sea";
import { useRouter } from "next/router";
import { GunDB } from "../../gundb";
import { GunUser, ISEAPair, OptionsUserAuth } from "gun";

export interface ResponseLogin {
  ack?: number;
  /** ~publicKeyOfUser */
  soul?: string;
  /** ~publicKeyOfUser */
  get?: string;
  put?: GunUser;
  sea?: ISEAPair;
  pair?: ISEAPair;
  options?: OptionsUserAuth ;
  err?: string;
}
const Login: NextPage = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [account, setAccount] = useState({});
  const router = useRouter();

  useEffect(()=>{
    GunDB.get('text').on(text => setAccount({text}));
  },[])

  const userLogin = () => {
    GunDB.user().auth(username, password, (data: ResponseLogin) => {
      console.log(data)
      if(data.sea?.pub!==""){
        GunDB.get('text').put(data);
        router.push("/secure")
      }else{
        alert(data.err)
      }
    });
  };
  return (
    <div className="container mx-auto p-28 space-y-2 content-center">
      <div className="bg-gray-100 rounded-sm">
        <span className="block text-sm font-medium text-center text-slate-700 italic">
          Username/Email
        </span>
        <input
          onChange={(e) => setUsername(e.currentTarget.value)}
          value={username}
          type={"text"}
          placeholder="username"
          className="flex-1 text-center text-slate-800 active:bg-transparent focus:bg-transparent text-lg w-full bg-transparent active:outline-none focus:outline-none"
        />
      </div>
      <div className="bg-gray-100 rounded-sm">
        <span className="block text-sm font-medium text-center text-slate-700 italic">
          Password
        </span>
        <input
          onChange={(e) => setPassword(e.currentTarget.value)}
          value={password}
          type={"password"}
          placeholder="* * * * *"
          className="flex-1 text-center text-slate-800 active:bg-transparent focus:bg-transparent text-lg w-full bg-transparent active:outline-none focus:outline-none"
        />
      </div>
      <div className="content-center float-right">
        <button
          className="bg-cyan-700 rounded-sm w-80"
          onClick={() => userLogin()}
        >
          Sign
        </button>
      </div>
    </div>
  );
};

export default Login;
